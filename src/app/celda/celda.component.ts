import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-celda',
  templateUrl: './celda.component.html',
  styleUrls: ['./celda.component.css']
})
export class CeldaComponent implements OnInit {



  @Input() color;

  constructor() { }

  ngOnInit() {
  }

}
