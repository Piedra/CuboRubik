import { TestBed, inject } from '@angular/core/testing';

import { LogicaService } from './logica.service';

describe('LogicaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogicaService]
    });
  });

  it('should be created', inject([LogicaService], (service: LogicaService) => {
    expect(service).toBeTruthy();
  }));
});
