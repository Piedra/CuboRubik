import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CuboComponent } from './cubo/cubo.component';
import { CeldaComponent } from './celda/celda.component';
import { CaraComponent } from './cara/cara.component';

@NgModule({
  declarations: [
    AppComponent,
    CuboComponent,
    CeldaComponent,
    CaraComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
