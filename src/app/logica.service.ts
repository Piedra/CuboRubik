import { Injectable } from '@angular/core';
import { Cubo } from './cubo';
import { CaraClass } from './cara-class';
import { Celda } from './celda';
import { Movimiento } from './movimiento';

@Injectable({
  providedIn: 'root'
})
export class LogicaService {

  listaMovimientos: Array<Movimiento>;
  matriz: object[][];

  constructor() {
    this.listaMovimientos = new Array<Movimiento>();
  }


  getLista() {
    return JSON.parse(localStorage.getItem('Movimientos') || '[]');
  }


  ingresarMovimiento(movimiento: Movimiento) {
    this.listaMovimientos = JSON.parse(localStorage.getItem('Movimientos') || '[]');
    this.listaMovimientos.push(movimiento);
    this.guardarLista(this.listaMovimientos);
  }


  guardarLista(arreglo: Array<Movimiento>) {
    localStorage.removeItem('Movimientos');
    localStorage.setItem('Movimientos', JSON.stringify(arreglo));
  }

  borrarMovimiento() {
    this.listaMovimientos = JSON.parse(localStorage.getItem('Movimientos') || '[]');
    if (this.listaMovimientos.length > 0) {
      this.listaMovimientos.pop();
      this.listaMovimientos.pop();
    }
    this.guardarLista(this.listaMovimientos);
  }

  borrarLista() {
    this.listaMovimientos = new Array<Movimiento>();
    this.guardarLista(this.listaMovimientos);
  }

  guardarCubo(caras: CaraClass[]) {
    localStorage.setItem('Cubo', JSON.stringify(caras));
  }

  getCubo() {
    const list = JSON.parse(localStorage.getItem('Cubo') || '[]');
    if (list.length > 0) {
      return list;
    } else {
      return new CaraClass([], 1).generaCaras();
    }
  }

}
