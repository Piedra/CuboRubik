import { Component, OnInit, Input } from '@angular/core';
import { Celda } from '../celda';

@Component({
  selector: 'app-cara',
  templateUrl: './cara.component.html',
  styleUrls: ['./cara.component.css']
})
export class CaraComponent implements OnInit {

  @Input() cara;
  constructor() { }

  ngOnInit() {
  }


  nombreCara() {
    switch (this.cara.id) {
      case 0:
        return 'uno';
      case 1:
        return 'dos';
      case 2:
        return 'tres';
      case 3:
        return 'cuatro';
      case 4:
        return 'cinco';
      case 5:
        return 'seis';

    }
  }

  recorrerCeldas() {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.cara.id === 0 && this.cara.celdas[0][0].id === 0) {
          /**this.cara.celdas[0][0].color = 'azul';**/
          return this.numeroCelda(this.cara.celdas[0][0]);
        }
      }
    }
  }


  numeroCelda(celda) {
    if (celda === this.cara.celdas[0][0]) {
      return this.cara.celdas[0][0].color;
    } else if (celda === this.cara.celdas[0][1]) {
      return;
    } else if (celda === this.cara.celdas[0][2]) {
      return this.cara.celdas[0][2].color;
    } else if (celda === this.cara.celdas[1][0]) {
      return this.cara.celdas[1][0].color;
    } else if (celda === this.cara.celdas[1][1]) {
      return this.cara.celdas[1][1].color;
    } else if (celda === this.cara.celdas[1][2]) {
      return this.cara.celdas[1][2].color;
    } else if (celda === this.cara.celdas[2][0]) {
      return this.cara.celdas[2][0].color;
    } else if (celda === this.cara.celdas[2][1]) {
      return this.cara.celdas[2][1].color;
    } else if (celda === this.cara.celdas[2][2]) {
      return this.cara.celdas[2][2].color;
    }
  }


}
