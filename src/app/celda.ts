export class Celda {
    color: string;
    id: number;
    constructor(color: string, id: number) {
        this.color = color;
        this.id = id;
    }

}
