import { CaraClass } from './cara-class';
import { Movimiento } from './movimiento';
import {LogicaService} from './logica.service';
import { Celda } from './celda';

export class Cubo {
    caras: CaraClass[];
    listaMovimientos: Array<Movimiento>;
    servLog: LogicaService;
    colorCara0: string[]; /**Cada variable llamada ColorCara son para tomar el color */
    colorCara1: string[]; /**De la cara Sucesora a cambiar*/
    colorCara2: string[];
    colorCara3: string[];
    colorCara4: string[];
    colorCara5: string[];
    x: number;
    y: number;


    constructor() {
        this.servLog = new LogicaService();
        this.caras = this.servLog.getCubo();
        this.colorCara0 = [];
        this.colorCara1 = [];
        this.colorCara2 = [];
        this.colorCara3 = [];
        this.colorCara4 = [];
        this.colorCara5 = [];
        this.listaMovimientos = new Array<Movimiento>();
    }

    movimientoDerecha(opc: number) {
        this.recogerColoresLados(opc);
        switch (opc) {
            case 0:
                this.movimientoLados(this.colorCara3, 0, 1, opc);
                this.movimientoLados(this.colorCara0, 1, 2, opc);
                this.movimientoLados(this.colorCara1, 2, 1, opc);
                this.movimientoLados(this.colorCara2, 3, 2, opc);
                this.movimientoEspecialDerecha(this.colorCara4, 4);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 1));
                this.servLog.guardarCubo(this.caras);
                break;
            case 1:
                this.movimientoLados(this.colorCara3, 0, 1, opc);
                this.movimientoLados(this.colorCara0, 1, 2, opc);
                this.movimientoLados(this.colorCara1, 2, 1, opc);
                this.movimientoLados(this.colorCara2, 3, 2, opc);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 2));
                this.servLog.guardarCubo(this.caras);
                break;
            case 2:
                this.movimientoLados(this.colorCara3, 0, 1, opc);
                this.movimientoLados(this.colorCara0, 1, 2, opc);
                this.movimientoLados(this.colorCara1, 2, 1, opc);
                this.movimientoLados(this.colorCara2, 3, 2, opc);
                this.movimientoEspecialDerecha(this.colorCara5, 5);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 3));
                this.servLog.guardarCubo(this.caras);
                break;

        }
    }
    /**Ejecuta una serie de metodos dependiendo de la opción solicita por parametro,
   * los metodos harian posible el movimiento de la en sobre filas desplazandolas hacia Izquierda
   * 1) Desplaza las filas hacia la Izquierda y realiza el movimiento de la cara de Arriba
   * 2) Desplaza las filas hacia la Izquierda.
   * 3) Desplaza las filas hacia la Izquierda y realiza el movimiento de la cara de Abajo
   */
    movimientoIzquierda(opc: number) {
        this.recogerColoresLados(opc);
        switch (opc) {
            case 0:
                this.movimientoLados(this.colorCara1, 0, 4, opc);
                this.movimientoLados(this.colorCara2, 1, 5, opc);
                this.movimientoLados(this.colorCara3, 2, 4, opc);
                this.movimientoLados(this.colorCara0, 3, 5, opc);
                this.movimientoIzquierdaAA(this.colorCara4, 4);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 4));
                this.servLog.guardarCubo(this.caras);
                break;
            case 1:
                this.movimientoLados(this.colorCara1, 0, 4, opc);
                this.movimientoLados(this.colorCara2, 1, 5, opc);
                this.movimientoLados(this.colorCara3, 2, 4, opc);
                this.movimientoLados(this.colorCara0, 3, 5, opc);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 5));
                this.servLog.guardarCubo(this.caras);
                break;
            case 2:
                /**Analizar aca */
                this.movimientoLados(this.colorCara1, 0, 4, opc);
                this.movimientoLados(this.colorCara2, 1, 5, opc);
                this.movimientoLados(this.colorCara3, 2, 4, opc);
                this.movimientoLados(this.colorCara0, 3, 5, opc);
                this.movimientoIzquierdaAA(this.colorCara5, 5);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 6));
                this.servLog.guardarCubo(this.caras);
                break;

        }
    }
    /**Ejecuta una serie de metodos dependiendo de la opción solicita por parametro,
   * los metodos harian posible el movimiento de la matriz hacia Izquierda
   * 1) Desplaza las columnas hacia la Arriba y realiza el movimiento de la cara de Izquierda
   * 2) Desplaza las columnas hacia la Arriba.
   * 3) Desplaza las columnas hacia la Arriba y realiza el movimiento de la cara de Derecha
   */
    movimientoArriba(opc: number) {
        this.recogerColoresTapas(opc + 3);
        switch (opc) {
            case 0:
                this.movimientoLados(this.colorCara5, 0, 6, opc);
                this.movimientoLados(this.colorCara0, 4, 7, opc);
                this.movimientoLados(this.colorCara4, 2, 6, opc);
                this.movimientoLados(this.colorCara2, 5, 7, opc);
                this.movimientoIzquierdaAA(this.colorCara3, 3);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 7));
                this.servLog.guardarCubo(this.caras);
                break;
            case 1:
                this.movimientoLados(this.colorCara5, 0, 6, opc);
                this.movimientoLados(this.colorCara0, 4, 7, opc);
                this.movimientoLados(this.colorCara4, 2, 6, opc);
                this.movimientoLados(this.colorCara2, 5, 7, opc);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 8));
                this.servLog.guardarCubo(this.caras);
                break;
            case 2:
                this.movimientoLados(this.colorCara5, 0, 6, opc);
                this.movimientoLados(this.colorCara0, 4, 7, opc);
                this.movimientoLados(this.colorCara4, 2, 6, opc);
                this.movimientoLados(this.colorCara2, 5, 7, opc);
                this.movimientoIzquierdaAA(this.colorCara1, 1);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 9));
                this.servLog.guardarCubo(this.caras);
                break;
        }
    }
    /**Ejecuta una serie de metodos dependiendo de la opción solicita por parametro,
   * los metodos harian posible el movimiento de la matriz hacia Izquierda
   * 1) Desplaza las columnas hacia la Arriba y realiza el movimiento de la cara de Izquierda
   * 2) Desplaza las columnas hacia la Arriba.
   * 3) Desplaza las columnas hacia la Arriba y realiza el movimiento de la cara de Derecha
   */
    movimientoAbajo(opc: number) {
        this.recogerColoresTapas(opc + 3);
        switch (opc) {
            case 0:
                this.movimientoLados(this.colorCara4, 0, 8, opc);
                this.movimientoLados(this.colorCara2, 4, 9, opc);
                this.movimientoLados(this.colorCara5, 2, 8, opc);
                this.movimientoLados(this.colorCara0, 5, 9, opc);
                this.movimientoEspecialDerecha(this.colorCara3, 3);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 10));
                this.servLog.guardarCubo(this.caras);
                break;
            case 1:
                this.movimientoLados(this.colorCara4, 0, 8, opc);
                this.movimientoLados(this.colorCara2, 4, 9, opc);
                this.movimientoLados(this.colorCara5, 2, 8, opc);
                this.movimientoLados(this.colorCara0, 5, 9, opc);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 11));
                this.servLog.guardarCubo(this.caras);
                break;
            case 2:
                this.movimientoLados(this.colorCara4, 0, 8, opc);
                this.movimientoLados(this.colorCara2, 4, 9, opc);
                this.movimientoLados(this.colorCara5, 2, 8, opc);
                this.movimientoLados(this.colorCara0, 5, 9, opc);
                this.movimientoEspecialDerecha(this.colorCara1, 1);
                this.servLog.ingresarMovimiento(new Movimiento(opc, 12));
                this.servLog.guardarCubo(this.caras);
                break;
        }
    }
    /*Recorre el Cubo preguntado por cada una de las caras,y toma los colores de sus filas.
    en el caso de la cara de arriba y la cara de abajo toma todos los colores exepto el del medio.
    */
    recogerColoresLados(opc: number) {
        this.caras.forEach(element => {
            if (element.id === 0) {
                this.colorCara0 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 1) {
                this.colorCara1 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 2) {
                this.colorCara2 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 3) {
                this.colorCara3 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 4) {
                this.colorCara4 = this.tomarColores(element.celdas, 6);
            } else if (element.id === 5) {
                this.colorCara5 = this.tomarColores(element.celdas, 6);
            }
        });
    }
    /** Recorre el Cubo preguntado por cada una de las caras,y toma los colores de sus columnas.
    en el caso de la cara de derecha y Izquierda toma todos los colores exepto el del medio. */
    recogerColoresTapas(opc: number) {
        this.caras.forEach(element => {
            if (element.id === 0) {
                this.colorCara0 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 1) {
                this.colorCara1 = this.tomarColores(element.celdas, 6);
            } else if (element.id === 2) {
                this.colorCara2 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 3) {
                this.colorCara3 = this.tomarColores(element.celdas, 6);
            } else if (element.id === 4) {
                this.colorCara4 = this.tomarColores(element.celdas, opc);
            } else if (element.id === 5) {
                this.colorCara5 = this.tomarColores(element.celdas, opc);
            }
        });
    }

    /*Recibe una matriz de celdas que en este caso seria una cara
      y dependiendo del parametro toma los colores de una forma especifica.
    */
    tomarColores(celdas: Celda[][], opcion: number) {
        const colores = [];
        switch (opcion) {
            case 0:
                colores.push(celdas[0][0].color);
                colores.push(celdas[0][1].color);
                colores.push(celdas[0][2].color);
                return colores;
            case 1:
                colores.push(celdas[1][0].color);
                colores.push(celdas[1][1].color);
                colores.push(celdas[1][2].color);
                return colores;
            case 2:
                colores.push(celdas[2][0].color);
                colores.push(celdas[2][1].color);
                colores.push(celdas[2][2].color);
                return colores;
            case 3:
                colores.push(celdas[0][0].color);
                colores.push(celdas[1][0].color);
                colores.push(celdas[2][0].color);
                return colores;
            case 4:
                colores.push(celdas[0][1].color);
                colores.push(celdas[1][1].color);
                colores.push(celdas[2][1].color);
                return colores;
            case 5:
                colores.push(celdas[0][2].color);
                colores.push(celdas[1][2].color);
                colores.push(celdas[2][2].color);
                return colores;
            case 6:
                colores.push(celdas[0][0].color);
                colores.push(celdas[0][1].color);
                colores.push(celdas[0][2].color);
                colores.push(celdas[2][0].color);
                colores.push(celdas[2][1].color);
                colores.push(celdas[2][2].color);
                colores.push(celdas[1][0].color);
                colores.push(celdas[1][2].color);
                return colores;
        }
    }

    /**Tiene la mayoria de movimientos del cubo. Recibe por parametro el ID: id de la cara,
     * Caso: Opcion de cambio 1 y 2: Derecha, 4,5: Izquierda, 6,7: Arriba, 8,9 Abajo
     * OPC: Cambia segun el caso, en el de los lados es la fila, y en el de Arriba y abajo, son las comlumnas*/
    movimientoLados(colorCara: string[], caraId: number, caso: number, opc: number) {
        switch (caso) {
            case 1:
                this.caras[caraId].celdas[opc][2].color = colorCara[0];
                this.caras[caraId].celdas[opc][1].color = colorCara[1];
                this.caras[caraId].celdas[opc][0].color = colorCara[2];
                break;
            case 2:
                this.caras[caraId].celdas[opc][0].color = colorCara[0];
                this.caras[caraId].celdas[opc][1].color = colorCara[1];
                this.caras[caraId].celdas[opc][2].color = colorCara[2];
                break;
            case 4:
                this.caras[caraId].celdas[opc][2].color = colorCara[2];
                this.caras[caraId].celdas[opc][1].color = colorCara[1];
                this.caras[caraId].celdas[opc][0].color = colorCara[0];
                break;
            case 5:
                this.caras[caraId].celdas[opc][0].color = colorCara[2];
                this.caras[caraId].celdas[opc][1].color = colorCara[1];
                this.caras[caraId].celdas[opc][2].color = colorCara[0];
                break;
            case 6:
                this.caras[caraId].celdas[2][opc].color = colorCara[0];
                this.caras[caraId].celdas[1][opc].color = colorCara[1];
                this.caras[caraId].celdas[0][opc].color = colorCara[2];
                break;
            case 7:
                this.caras[caraId].celdas[0][opc].color = colorCara[0];
                this.caras[caraId].celdas[1][opc].color = colorCara[1];
                this.caras[caraId].celdas[2][opc].color = colorCara[2];
                break;
            case 8:
                this.caras[caraId].celdas[2][opc].color = colorCara[2];
                this.caras[caraId].celdas[1][opc].color = colorCara[1];
                this.caras[caraId].celdas[0][opc].color = colorCara[0];
                break;
            case 9:
                this.caras[caraId].celdas[0][opc].color = colorCara[2];
                this.caras[caraId].celdas[1][opc].color = colorCara[1];
                this.caras[caraId].celdas[2][opc].color = colorCara[0];
                break;
        }
    }
    /**Es un movimiento unico, que se hace en la Matriz. Sucede cuando se Afectan dos caras al mismo tiempo
     * en este caso seria el movimiento epecial hacia la derecha
    */
    movimientoEspecialDerecha(colorCara: string[], caraId: number) {
        this.caras[caraId].celdas[0][0].color = colorCara[2];
        this.caras[caraId].celdas[0][1].color = colorCara[7];
        this.caras[caraId].celdas[0][2].color = colorCara[5];
        this.caras[caraId].celdas[2][0].color = colorCara[0];
        this.caras[caraId].celdas[2][1].color = colorCara[6];
        this.caras[caraId].celdas[2][2].color = colorCara[3];
        this.caras[caraId].celdas[1][0].color = colorCara[1];
        this.caras[caraId].celdas[1][2].color = colorCara[4];
    }
    /**Es un movimiento unico, que se hace en la Matriz. Sucede cuando se Afectan dos caras al mismo tiempo
   * en este caso seria el movimiento epecial hacia la Izquierda
  */
    movimientoIzquierdaAA(colorCara: string[], caraId: number) {
        this.caras[caraId].celdas[0][0].color = colorCara[3];
        this.caras[caraId].celdas[0][1].color = colorCara[6];
        this.caras[caraId].celdas[0][2].color = colorCara[0];
        this.caras[caraId].celdas[2][0].color = colorCara[5];
        this.caras[caraId].celdas[2][1].color = colorCara[7];
        this.caras[caraId].celdas[2][2].color = colorCara[2];
        this.caras[caraId].celdas[1][0].color = colorCara[4];
        this.caras[caraId].celdas[1][2].color = colorCara[1];
    }

    /**Metodo que toma una lista del movimientos almacenados en el LocalStorage
     * por medio de un Service y le da vuelta y lo reccore alrevez
     * perguntando por cada uno de los movimientos para hacer el opuesto
     * y lograr un efecto de Devolverse
     */
    undoMovimiento() {
        const BreakException = {};
        this.listaMovimientos = this.servLog.getLista();
        try {
            const listaAlrevez = this.listaMovimientos.reverse();
            listaAlrevez.forEach(movimiento => {
                if (movimiento.opcion > 0 && movimiento.opcion <= 3) {
                    this.movimientoIzquierda(movimiento.fColumna);
                    this.servLog.borrarMovimiento();
                    throw BreakException;
                } else if (movimiento.opcion > 3 && movimiento.opcion <= 6) {
                    this.movimientoDerecha(movimiento.fColumna);
                    this.servLog.borrarMovimiento();
                    throw BreakException;
                } else if (movimiento.opcion > 6 && movimiento.opcion <= 9) {
                    this.movimientoAbajo(movimiento.fColumna);
                    this.servLog.borrarMovimiento();
                    throw BreakException;
                } else if (movimiento.opcion > 9 && movimiento.opcion <= 12) {
                    this.movimientoArriba(movimiento.fColumna);
                    this.servLog.borrarMovimiento();
                    throw BreakException;
                }
            });
        } catch (error) {
        }
    }
    /**Metodo que vulve las caras al color principal con el que comenzo */
    restablecerCubo() {
        this.caras = new CaraClass([], 1).generaCaras();
        this.servLog.borrarLista();
        this.servLog.guardarCubo(this.caras);
    }


}
