import { Component, OnInit, Input } from '@angular/core';
import { Cubo } from '../cubo';



@Component({
  selector: 'app-cubo',
  templateUrl: './cubo.component.html',
  styleUrls: ['./cubo.component.css']
})
export class CuboComponent implements OnInit {

  @Input() cara;

  private x: number;
  private y: number;
  private fila: number;
  private cubo: Cubo;
  private flag: boolean;
  private cont: number;
  private res: boolean;


  constructor() {
    this.cubo = new Cubo();
    this.x = 0;
    this.y = 0;
    this.flag = false;
    this.res = false;

  }

  ngOnInit() {
  }

  myEvent(event) {
    if (this.flag === true) {
      this.x -= (event.clientY + this.x) - 800;
      this.y -= (event.clientX + this.y)  - 800;
    }
  }

  activate_flag() {
    this.cont = 0;
    if (this.cont === 0) {
      this.cont++;
      this.flag = true;
    }
  }

  disable_flag() {
    if (this.cont > 0) {
      this.cont = 0;
      this.flag = false;
    }
  }

  event_xUp() {
    this.x += 90;
    return this.x;
  }

  event_xDown() {
    this.x -= 90;
    return this.x;
  }

  event_y_Left() {
    this.y -= 90;
    return this.y;
  }

  event_y_Right() {
    this.y += 90;
    return this.y;
  }

  /**Funcion que se llama en el HTML para mover las Caras Hacia la Derecha
   * Se toma el valor de la Fila por medio de un input y se valida en la Función
  */
  moverDerecha() {
    if (this.fila > 3) {
      this.res = true;
    } else if (this.fila > 0 && this.fila < 2) {
      this.cubo.movimientoDerecha(0);
    } else if (this.fila > 1 && this.fila < 3) {
      this.cubo.movimientoDerecha(1);
    } else if (this.fila > 2 && this.fila < 4) {
      this.cubo.movimientoDerecha(2);
    } else if (this.fila <= 0) {
      this.res = true;
    }
  }
  /**Funcion que se llama en el HTML para mover las Caras Hacia la Izquierda
   * Se toma el valor de la Fila por medio de un input y se valida en la Función
  */
  moverIzquierda() {
    if (this.fila > 3) {
      this.res = true;
    } else if (this.fila > 0 && this.fila < 2) {
      this.cubo.movimientoIzquierda(0);
    } else if (this.fila > 1 && this.fila < 3) {
      this.cubo.movimientoIzquierda(1);
    } else if (this.fila > 2 && this.fila < 4) {
      this.cubo.movimientoIzquierda(2);
    } else if (this.fila <= 0) {
      this.res = true;
    }
  }
  /**Funcion que se llama en el HTML para mover las Caras a la Hacia Arriba
     * Se toma el valor de la Fila por medio de un input y se valida en la Función
    */
  moverArriba() {
    if (this.fila > 3) {
      this.res = true;
    } else if (this.fila > 0 && this.fila < 2) {
      this.cubo.movimientoArriba(0);
    } else if (this.fila > 1 && this.fila < 3) {
      this.cubo.movimientoArriba(1);
    } else if (this.fila > 2 && this.fila < 4) {
      this.cubo.movimientoArriba(2);
    } else if (this.fila <= 0) {
      this.res = true;
    }
  }
  /**Funcion que se llama en el HTML para mover las Caras a la Hacia Abajo
     * Se toma el valor de la Fila por medio de un input y se valida en la Función
    */
  moverAbajo() {
    if (this.fila > 3) {
      this.res = true;
    } else if (this.fila > 0 && this.fila < 2) {
      this.cubo.movimientoAbajo(0);
    } else if (this.fila > 1 && this.fila < 3) {
      this.cubo.movimientoAbajo(1);
    } else if (this.fila > 2 && this.fila < 4) {
      this.cubo.movimientoAbajo(2);
    } else if (this.fila <= 0) {
      this.res = true;
    }
  }

  /**Llama al metodo undo Movimiento de la clase Cubo */
  undoMovimiento() {
    this.cubo.undoMovimiento();
  }

  /**Llama al metodo Restablecer el cuo de la clase Cubo, que se
   * encargar de restablecer el cubo a sus caras originales
   */
  restablecerCubo() {
    this.x = 0;
    this.y = 0;
    this.cubo.restablecerCubo();
  }
  /**Boolean para el impirmir o no un mensaje indicando que se sale del margen
   * columnas o filas
  */
  label_visible() {
    this.res = false;
  }

  /**Coloca el cubo en la posicion 0 */
  reFocus() {
    this.x = 0;
    this.y = 0;
  }

}
