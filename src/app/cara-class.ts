import { Celda } from './celda';

export class CaraClass {
    id: number;
    celdas: Celda[][];

    constructor(celdas: Celda[][], id) {
        this.celdas = celdas;
        this.id = id;
    }

    generarMat(color) {
        const arr2D = [
          [new Celda(color, 0), new Celda(color, 1), new Celda(color, 2)],
          [new Celda(color, 3), new Celda(color, 4), new Celda(color, 5)],
          [new Celda(color, 6), new Celda(color, 7), new Celda(color, 8)]
        ];
        return arr2D;
      }


      generaCaras() {
        const lista = [];
        const listaCol = ['amarillo', 'azul', 'blanco', 'verde', 'rojo', 'anaranjado'];
        for (let i = 0; i < 6; i++) {
          const cara = new CaraClass(this.generarMat(listaCol[i]), i);
          lista.push(cara);
        }
        return lista;
      }

}
